import Foundation
import UIKit

class Validator {
    
    func validate(text: String) -> String? {
        if let result = Rule.validatorWordLength.check(text) {
            return result
        }
        if Rule.validatorHaveAt.check(text) != nil {
            return Rule.validEmail.check(text)
        } else {
            let rules = [Rule.validatorFirstLetter,.validatorNickname]
            return rules.compactMap({ $0.check(text) }).first
        }
    }
    
    func validateTwoVersion(text: String) -> String? {
        var rules = [Rule]()
        if Rule.validatorHaveAt.check(text) != nil {
            rules = [Rule.validatorWordLength,.validEmail]
        } else {
            rules = [Rule.validatorWordLength,.validatorFirstLetter,.validatorNickname]
        }
        return rules.compactMap({ $0.check(text) }).first
    }
}


