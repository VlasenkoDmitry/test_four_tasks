import UIKit
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var login: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func checkLogin() {
        if let text = login.text {
            if let result = validator.validate(text: text) {
                showAlert(title:"Fail", text: result)
            } else {
                showAlert(title:"Success", text:"login corresponds to the rules")
            }
        } else {
            showAlert(title:"Fail", text: "empty login")
        }
    }
    
    func showAlert(title: String,text :String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.view.tintColor = .red
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        login.resignFirstResponder()
        checkLogin()
        return true
    }
}
