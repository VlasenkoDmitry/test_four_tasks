import UIKit

class DemoTableViewCell: UITableViewCell {

    @IBOutlet weak var newImageView: UIImageView!
        
    func configure (image:UIImage){
        newImageView.image = image
    }
    
    override func prepareForReuse() {
        newImageView.image = nil
    }
}
