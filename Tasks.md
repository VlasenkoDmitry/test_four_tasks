Задание 1 
Напишите приложение для мобильных устройств.
1. Устройство iPhone, только портретная ориентация.
2. Минимальная поддерживаемая версия iOS: iOS 12.0
3. Язык программирования Swift: последней версии
4. Необходимо реализовать метод загрузки картинки в ячейку таблицы.
a. Сигнатура метода: func downloadImage(withURL url: URL, forCell cell: UITableViewCell).
b. Таблица должна выводить список из 100 картинок.
c. Ссылка на картинку формируется так: : http://placehold.it/375x150?text={index}, где index -
порядковый номер ячейки (например, http://placehold.it/375x150?text=1, http://placehold.it/375x150?text=2, и т.д.).
5. В данной задаче запрещается использовать сторонние библиотеки.
6. Для загрузки изображения необходимо использовать URLSession.


Задание 2
Объясните почему блок "Work 2" не выполняется?
let serialQueue = DispatchQueue(label: "com.foo.bar")
serialQueue.async {
    print("Work 1") 
    serialQueue.sync {
        print("Work 2")
    }
}

Задание 3
Напишите консольное приложение.
1. Оно должно запрашивать никнейм и получать по нему список репозиториев из Github.
2. Полученный список репозиториев выводится на экран терминала в виде списка имен репозиториев.
3. Для запросов в сеть нужно использовать Alamofire с помощью swift package manager.
4. Для доступа к Github использовать официальный API https://developer.github.com/v3/.
5. Язык программирования: Swift последней версии.

Задание 4
Напишите приложение для мобильных устройств.
1. Устройство iPhone, только портретная ориентация.
2. Минимальная поддерживаемая версия iOS: iOS 12.0
3. Язык программирования Swift: последней версии
4. Которое проверяет, соответствует ли введенный в текстовое поле логин следующим правилам:
a. Логин может быть как email-ом так и обычной строкой (никнейм).
b. Минимальная длина логина - 3 символа, максимальная - 32.
c. Логин может состоять из латинских букв, цифр, минуса и точки.
d. Логин не может начинаться на цифру, точку, минус.
5. Код должен быть покрыт Unit-тестами.
