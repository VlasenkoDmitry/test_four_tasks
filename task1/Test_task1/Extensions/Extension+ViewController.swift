import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants().numberRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DemoTableViewCell", for: indexPath) as? DemoTableViewCell else { return UITableViewCell() }
        if let url = Urlgeneration().createURL(index: indexPath, urlText: Constants().urlText) {
            API.shared.downloadImage(withURL: url, forCell: cell)
        }
        return cell
    }
}
