import Foundation
import Alamofire

class API {
    
    static let shared = API()
    
    func requestGit(name: String,repositoryLoadingGroup: DispatchGroup,page: Int, completion: @escaping ([Repository])->()){
        
        repositoryLoadingGroup.enter()
        if let url = URL(string: "https://api.github.com/users/"+"\(name)"+"/repos"+"?per_page="+"\(Constants().perPage)"+"&page="+"\(page)") {
            AF.request(url).responseJSON(queue: .global()) { response in
                guard let data = response.data else { return }
                var object = [Repository]()
                do{
                    object = try JSONDecoder().decode([Repository].self, from: data)
                    if object.count == Constants().perPage {
                        self.requestGit(name: name,repositoryLoadingGroup: repositoryLoadingGroup,page: page + 1) { newRequestResult in
                            completion (newRequestResult)
                        }
                    }
                }
                catch{
                    print("Data err")
                }
                completion (object)
            }
        }
    }
}

