import Foundation

class Urlgeneration {
    
    func createURL(index: IndexPath,urlText: String) -> URL? {
        let stringUrl = urlText.replacingOccurrences(of: "{index}", with: "\(index.row + 1)", options: NSString.CompareOptions.literal, range: nil)
        guard let url = URL(string: stringUrl) else { return nil }
        return url
    }
}
