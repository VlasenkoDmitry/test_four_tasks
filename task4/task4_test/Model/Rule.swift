import Foundation

struct Rule {
    
    let check: (String) -> String?
    
    static let validatorWordLength = Rule(check: {
        return $0.count >= 3 && $0.count <= 32 ? nil : "Min 3 Max 32 letters"
    })
    
    static let validatorHaveAt = Rule(check: {
        return $0.contains("@") ? "Dont Have @" : nil
    })
    
    static let validEmail = Rule(check: {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.([A-Za-z]){2,}").evaluate(with: $0) ? nil : "Must have valid email"
    })

    static let validatorFirstLetter = Rule(check: {
        return NSPredicate(format: "SELF MATCHES %@", "[0-9-.]").evaluate(with: String(Array($0)[0])) ? "Must have valid first letter in nickname" : nil
    })
    
    static let validatorNickname = Rule(check: {
        return NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z-.]{1,}").evaluate(with: $0) ? nil : "Must have valid nickname"
    })
}
