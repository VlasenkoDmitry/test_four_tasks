import Foundation
import UIKit

class API {
    
    static let shared = API()
    
    func downloadImage(withURL url: URL, forCell cell: UITableViewCell) {
        guard let cell = cell as? DemoTableViewCell else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error == nil, let data = data {
                guard let image = UIImage(data: data) else { return }
                DispatchQueue.main.async {
                    cell.configure(image: image)
                }
            }
        }
        task.resume()
    }
}
