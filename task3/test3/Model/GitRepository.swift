import Foundation
import Alamofire

class GitRepository {
    
    func gitRepositoryName() {
        var repositoryNames = [Repository]()
        print("Enter nickname")
        if let response = readLine() {
            if response.contains(" ") || response.count == 0 {
                print("incorrect nickname")
                gitRepositoryName()
            } else {
                let repositoryLoadingGroup = DispatchGroup()
                API.shared.requestGit(name: response,repositoryLoadingGroup: repositoryLoadingGroup,page: 1) { repositoryNamesNew in
                    repositoryNames += repositoryNamesNew
                    repositoryLoadingGroup.leave()
                }
                repositoryLoadingGroup.wait()
                if repositoryNames.count == 0 {
                    print("no repositories were found for this nickname")
                } else {
                    for (index, repository) in repositoryNames.enumerated() {
                        if let name = repository.name {
                            print("\(index + 1) repositories is \(name)")
                        }
                    }
                }
                gitRepositoryName()
            }
        }
    }
}
